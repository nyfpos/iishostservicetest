﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
[ServiceContract]
public interface IService
{
    [WebInvoke(UriTemplate = "Add", Method = "POST", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml)]
    [OperationContract]
    OperateData Add(OperateData data); // This method takes no arguments, returns a string. Perfect for testing quickly with a browser.

}

[DataContract(Namespace = "http://localhost")]
public class OperateData
{
    [DataMember]
    public int lhs;
    [DataMember]
    public int rhs;
    [DataMember(IsRequired = false)]
    public int rs;

}